import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl:'./lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  constructor() { 
    this.destinos=[];
  }

  ngOnInit(): void {
  }
  guardar (nombre:string, url:string):boolean { 
    this.destinos.push(new DestinoViaje(nombre,url));//va creando el array
    return false;//para que no se refresque la pagina debido a que el boton es de tipo sumbit
  }

}
