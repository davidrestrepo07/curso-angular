import { Component, OnInit, Input, /*para tener variables externas*/ 
HostBinding} from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';//lo pone automaticamente

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destinos: DestinoViaje;
  @HostBinding('attr.class') cssClass='col-md-4';//le agrega esta clase al tag ficticio que se genera
  constructor() { }

  ngOnInit(): void {
  }

}
